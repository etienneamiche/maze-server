package main

import "testing"

func TestCalculateIncrement(t *testing.T){
	if CalculateIncrement(2) != 3 {
		t.Error("Expected 2 + 1 = 3")
	}
}

func TestTableCalculate(t *testing.T) {
	var tests = []struct {
		input    int
		expected int
	}{
		{2, 3},
		{-2, -1},
		{ 0, 1},
		{ 99999, 100000},
	}

	for _, test := range tests {
		if output := CalculateIncrement(test.input); output != test.expected {
			t.Error("Test Failed: {} inputed, {} expeted, received: {}", test.input,test.expected, output)
		}
	}
}