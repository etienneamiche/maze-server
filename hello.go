package main

import "fmt"

func main() {
	fmt.Println("Hello, World!")
	result := CalculateIncrement(2)
	fmt.Println(result)
}

func CalculateIncrement(x int) (result int) {
	result = x + 1
	return result
}
